<?php

/**
 * @file
 * Default image preset.
 */


/**
 * Implements hook_image_effect_info().
 */
function focal_point_no_upscale_image_effect_info() {
  $effects = array(
    'focal_point_scale_and_crop' => array(
      'label' => t('Focal Point Scale And Crop (No Upscaling)'),
      'help' => t('Scale and crop based on data provided by <em>Focal Point</em> with toggle to disable upscaling.'),
      'effect callback' => 'focal_point_no_upscale_scale_and_crop_effect',
      'dimensions callback' => 'focal_point_no_upscale_scale_and_crop_dimensions',
      'form callback' => 'focal_point_no_upscale_scale_and_crop_form',
      'summary theme' => 'focal_point_image_resize_summary',
    ),
  );

  return $effects;
}

/**
 * Image effect callback.
 */
function focal_point_no_upscale_scale_and_crop_effect(&$image, $data) {
  if ($data['upscale'] == 1) {
    $resize_data = focal_point_effect_resize_data($image->info['width'], $image->info['height'], $data['width'], $data['height']);
    if (!image_resize_effect($image, $resize_data)) {
      return FALSE;
    }
    if ($crop_data = focal_point_effect_crop_data($image, $data)) {
      // Next we crop.
      return image_crop_effect($image, $crop_data);
    }

    // At worst use the default effect and let Drupal handle the errors that
    // likely exist at this point.
    return image_scale_and_crop_effect($image, $data);
  }
  else {
    return image_scale_effect($image, $data);
  }
}

/**
 * Image dimensions callback; Scale and crop.
 *
 */
function focal_point_no_upscale_scale_and_crop_dimensions(array &$dimensions, array $data) {
  if($data['upscale'] == 1) {
    // The new image will have the exact dimensions defined for the effect.
    $dimensions['width'] = $data['width'];
    $dimensions['height'] = $data['height'];
  }
  else {
    if ($dimensions['width'] && $dimensions['height']) {
      image_dimensions_scale($dimensions, $data['width'], $data['height'], $data['upscale']);
    }
  }
}

/**
 * Form builder for the scale and crop forms.
 */
function focal_point_no_upscale_scale_and_crop_form($data = array()) {
  $form = image_scale_form($data);
  unset($form['anchor']);
  // Add input fields for focal point shifting.
  $form['focal_point_advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['focal_point_advanced']['shift_x'] = array(
    '#type' => 'textfield',
    '#title' => 'Horizontal Shift',
    '#description' => t('If set images will be cropped as though the focal point indicator has been shifted left (positive values) or shifted right (negative values) by the number of pixels specified.'),
    '#default_value' => (isset($data['focal_point_advanced']['shift_x']) ? $data['focal_point_advanced']['shift_x'] : ''),
    '#field_suffix' => t('pixels'),
    '#size' => 10,
    '#element_validate' => array('image_effect_integer_validate'),
    '#allow_negative' => TRUE,
  );
  $form['focal_point_advanced']['shift_y'] = array(
    '#type' => 'textfield',
    '#title' => 'Veritical Shift',
    '#description' => t('If set images will be cropped as though the focal point indicator has been shifted up (positive values) or shifted down (negative values) by the number of pixels specified.'),
    '#default_value' => (isset($data['focal_point_advanced']['shift_y']) ? $data['focal_point_advanced']['shift_y'] : ''),
    '#field_suffix' => t('pixels'),
    '#size' => 10,
    '#element_validate' => array('image_effect_integer_validate'),
    '#allow_negative' => TRUE,
  );

  return $form;
}
